﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBComm
{
    public static class DataAccess
    {
        public static List<T> LoadData<T, U>(string sql, U parameters, string connectionString)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            { 
                var rows = connection.Query<T>(sql, parameters);

                return rows.ToList();
            }
        }

        public static void SaveData<T>(string sql, T parameters, string connectionString)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                connection.Execute(sql, parameters);
            }
        }
    }
}
