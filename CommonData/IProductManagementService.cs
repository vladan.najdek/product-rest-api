﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonData
{
    public interface IProductManagementService
    {
        /// <summary>
        /// Retrieves all products from the product database
        /// </summary>
        /// <returns></returns>
        List<Product> GetAllProducts();

        /// <summary>
        /// Retrieves product with specific ID from the product database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Product GetProduct(int id);

        /// <summary>
        /// Updates product description in the database
        /// </summary>
        /// <param name="id">ID of the product to be updated</param>
        /// <param name="updatedProduct">Product with updated imformation</param>
        /// <returns>Returns true, if udated successfuly</returns>
        bool UpdateProduct(int id, Product updatedProduct);

        /// <summary>
        /// Retrieves products from specific page number based on pagination
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="pagination">Amount of products per page</param>
        /// <returns></returns>
        List<Product> GetPageOfProducts(int page, int pagination);
    }
}
