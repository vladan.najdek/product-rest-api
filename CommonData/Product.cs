using System;

namespace CommonData
{
    public class Product
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public string ImgUri { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }
    }
}
