﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonData;
using DBComm;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EshopAPI.Controllers.V2
{
    /// <summary>
    /// Controller for Products API
    /// </summary>
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ProductsController : ControllerBase
    {
        private IProductManagementService productManagement;

        /// <summary>
        /// Constructor for controller for Products API
        /// </summary>
        /// <param name="productManagementService"></param>
        public ProductsController(IProductManagementService productManagementService)
        {
            productManagement = productManagementService;
        }


        /// <summary>
        /// Retrieve all products form the product database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Product> Get()
        {
            return productManagement.GetAllProducts();
        }

        /// <summary>
        /// Retrieve product the with specific ID from the product database
        /// </summary>
        /// <param name="id">ID of the retrieved product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult Get(int id)
        {
            var result = productManagement.GetProduct(id);
            if (result == null)
            {
                return NotFound($"Couldn't find any product with id: {id}.");
            }
            return Ok(result);
        }

        /// <summary>
        /// Retrieves products based on page and pagination 
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="pagination">Number of items per page</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filtered")]
        public IActionResult Get(int page, int pagination = 10)
        {
            var result = productManagement.GetPageOfProducts(page, pagination);
            if (result.Count == 0)
            {
                return NotFound($"The given parameters of page: {page} and pagination: {pagination} give no results.");
            }
            return Ok(result);
        }

        /// <summary>
        /// Update product specification
        /// </summary>
        /// <param name="id">ID of product to change</param>
        /// <param name="product">New product data</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        public IActionResult Put(int id, [FromBody] Product product)
        {
            if (productManagement.UpdateProduct(id, product))
            {
                return Ok();
            }
            else
            {
                return NotFound($"Couldn't find any product with ID {id} to update.");
            }
        }
    }
}
