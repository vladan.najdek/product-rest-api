﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using CommonData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EshopAPI.Controllers.V1
{
    /// <summary>
    /// Controller for Products API
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        private IProductManagementService productManagement;

        /// <summary>
        /// Constructor for controller for Products API
        /// </summary>
        /// <param name="productManagementService"></param>
        public ProductsController(IProductManagementService productManagementService)
        {
            productManagement = productManagementService;
        }

        /// <summary>
        /// Retrieve all products form the product database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Product> Get()
        {
            return productManagement.GetAllProducts();
        }

        /// <summary>
        /// Retrieve product the with specific ID from the product database
        /// </summary>
        /// <param name="id">ID of the retrieved product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult Get(int id)
        {
            var result = productManagement.GetProduct(id);
            if (result == null)
            {
                return NotFound($"Couldn't find any product with id: {id}.");
            }
            return Ok(result);
        }

        /// <summary>
        /// Update product specification
        /// </summary>
        /// <param name="id">ID of product to change</param>
        /// <param name="product">New product data</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        public IActionResult Put(int id,[FromBody]Product product)
        {
            if (productManagement.UpdateProduct(id, product))
            {
                return Ok();
            }
            else
            {
                return NotFound($"Couldn't find any product with ID {id} to update.");
            }
        }
    }
}
