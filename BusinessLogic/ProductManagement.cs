﻿using DBComm;
using System;
using System.Collections.Generic;
using CommonData;
using System.Linq;
using System.Runtime.CompilerServices;

namespace BusinessLogic
{
    public class ProductManagement: IProductManagementService
    {
        const string sqlConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Database=EShop;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False";

        public List<Product> GetAllProducts()
        {
            return DataAccess.LoadData<Product, dynamic>("select * from Products", new { }, sqlConnectionString);
        }

        public Product GetProduct(int id)
        {
            return DataAccess.LoadData<Product, dynamic>($"select * from Products where Id={id}", new { }, sqlConnectionString).FirstOrDefault();
        }

        public bool UpdateProduct(int id, Product updatedProduct)
        {
            var product = DataAccess.LoadData<Product, dynamic>($"select * from Products where Id={id}", new { }, sqlConnectionString).FirstOrDefault();
            if (product == null) return false;
            DataAccess.SaveData<Product>($"update Products set Id=@ID,ProductName=@ProductName,ImgUri=@ImgUri,Price=@Price,Description=@Description where id={id}", updatedProduct, sqlConnectionString);
            return true;
        }

        public List<Product> GetPageOfProducts(int page, int pagination)
        {
            return DataAccess.LoadData<Product, dynamic>($"select * from (select Row_Number() over(order by Id) as RowNum, * from Products) t2 where RowNum between {1 + (page-1)*pagination} and {(page*pagination)}", new { }, sqlConnectionString);
        }
    }
}
