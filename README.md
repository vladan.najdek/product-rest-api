How to use:

1. Clone repository
2. Inside ProductDB project, double click ProductDB.publish.xml -> Publish, post-publish script automatically creates DB starting seed
3. Run the API project, default starting page is Swagger documentation
4. Unit test requires no additional setup, just run it