﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 1))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (1, N'IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Šetříme životní prostředí. Proto nový IPhone 13 neobsahuje v balení nabíječku, baterii, displej, kameru, procesor ani jiná zbytečná příslušenství.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 2))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (2, N'Nabíječka pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Nabíječka, která totálně není kompatibilní s tím kabelem, co máte doma.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 3))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (3, N'Baterie pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Baterie pro IPhone 13, IPhone 13S nebo IPhone 13XXL.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 4))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (4, N'Procesor pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'S převratným procesorem firmy Apple Vám ten IPhone možná i k něčemu bude.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 5))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (5, N'Kamera pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'To tady ještě nebylo! Nyní můžete se svým IPhone fotit nebo dokonce nahrávat video.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 6))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (6, N'Interní paměť pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Jste retro? Ze slov jako cloud nebo Google Drive se Vám dělá špatně? Pak je pro Vás určen tento modul interní paměti.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 7))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (7, N'USB-C kabel pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Jo, to je ten kabel, co nemáte doma.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 8))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (8, N'RAM pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Nechce se nám vysvětlovat, k čemu to je, ale taky to potřebujete.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 9))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (9, N'Displej pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Pokud ještě nemáte implantovaný grafický čip ve vašem cerebrálním cortexu, budete potřebovat tradiční 20" displej.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 10))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (10, N'IObal pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Toto není obyčejný obal. Je to IObal.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 11))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (11, N'Airpods pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Bluetooth sluchátka')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 12))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (12, N'Šňůrka pro IPhone 13 Airpods', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Ne, opravdu ty sluchátka nemohly mít kabel.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 13))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (13, N'Anténa pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Příslušenství pro lidi, kteří mají i nějaké kamarády a občas jim chtějí zavolat.')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 14))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (14, N'Licence IOS pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Roční licence na IOS')
IF (NOT EXISTS(SELECT * FROM dbo.Products WHERE Id = 15))
    INSERT INTO dbo.Products (Id, ProductName, ImgUri, Price, Description) VALUES (15, N'Návod k použití pro IPhone 13', 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=441&amp;hei=529&amp;fmt=jpeg&amp;qlt=95&amp;op_usm=0.5,0.5&amp;.v=1601844983000', 49999.99, N'Odborná literatura, pro úzkou skupinu uživatelů, kteří umějí číst.')
