﻿CREATE TABLE [dbo].[Products]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [ProductName] NVARCHAR(50) NULL, 
    [ImgUri] VARCHAR(1500) NULL, 
    [Price] DECIMAL(18, 2) NULL, 
    [Description] NVARCHAR(500) NULL
)
