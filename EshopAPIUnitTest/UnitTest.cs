using CommonData;
using EshopAPI.Controllers.V2;
using NUnit.Framework;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace EshopAPIUnitTest
{
    public class Tests
    {
        private MockProductManager mockManager;
        private ProductsController api;

        [SetUp]
        public void Setup()
        {
            mockManager = new MockProductManager();
            api = new ProductsController(mockManager);
        }

        [Test]
        public void GetAllProducts()
        {
            Assert.AreEqual(api.Get(), mockManager.Products, "Retrieved products are not same");
        }

        [Test]
        public void GetSpecificProductSuccessfuly()
        {
            Product randomMockProduct = mockManager.Products[new Random().Next(0, mockManager.Products.Count)];

            var result = api.Get(randomMockProduct.ID) as OkObjectResult;

            Assert.IsNotNull(result, "Operation doesn't have OK Result");
            Assert.AreEqual(200, result.StatusCode, "Retrieve operation has wrong result code");
            Assert.IsNotNull(result.Value, "Retrieved object is null");
            Assert.AreEqual(randomMockProduct, result.Value, "Retrieved some different product");
        }

        [Test]
        public void GetSpecificProductFail()
        {
            // Find some id that does not exist
            int id;
            do
            {
                id = new Random().Next();
            } while (mockManager.Products.Any(prod => prod.ID == id));

            var result = api.Get(id) as NotFoundObjectResult;

            Assert.IsNotNull(result, "Operation doesn't have NotFound Result");
            Assert.AreEqual(404, result.StatusCode, "Retrieve operation has wrong result code");
        }

        [Test]
        [TestCase(2,10)]
        [TestCase(3,2)]
        [TestCase(2,1)]
        public void GetPaginatedProductsSuccessfuly(int page, int pagination)
        {
            if (page*pagination > mockManager.Products.Count + pagination)
            {
                Assert.Inconclusive("Product collection too small");
                return;
            }

            List<Product> listToCompare = new List<Product>();
            for (int ii = (page - 1) * pagination; ii <= page * pagination -1; ii++)
            {
                if (mockManager.Products.ElementAtOrDefault(ii) != null) listToCompare.Add(mockManager.Products[ii]);
            }

            var result = api.Get(page, pagination) as OkObjectResult;
            var products = result.Value as List<Product>;

            Assert.IsNotNull(result, "Operation doesn't have OK Result");
            Assert.AreEqual(200, result.StatusCode, "Retrieve operation has wrong result code");
            Assert.IsNotNull(products, "Retrieved object is not list");
            Assert.AreNotEqual(products.Count, 0, "Retrieved empty collection");
            Assert.IsTrue(products.Count <= pagination, "Retrieved more products that should have retrieved");
            Assert.AreEqual(listToCompare, products, "Retrieved different products");
        }

        [Test]
        public void GetPaginatedProductsFail()
        {
            int page = mockManager.Products.Count + 1;
            int pagination = page + 1;
            var result = api.Get(page, pagination) as NotFoundObjectResult;

            Assert.IsNotNull(result, "Operation doesn't have NotFound Result");
            Assert.AreEqual(404, result.StatusCode, "Retrieve operation has wrong result code");
        }

        [Test]
        public void UpdateProductSuccessfuly()
        {
            int randomIndex = new Random().Next(0, mockManager.Products.Count);
            Product originalProduct = mockManager.Products[randomIndex];
            Product updatedProduct = new Product() { ID = 9000, ProductName = "Updated product", ImgUri = "NewUri", Price = 999.99f, Description = "NewDesc" };

            var result = api.Put(originalProduct.ID, updatedProduct) as OkResult;

            Assert.IsNotNull(result, "Operation doesn't have OK Result");
            Assert.AreEqual(200, result.StatusCode, "Retrieve operation has wrong result code");
            Assert.AreEqual(mockManager.Products[randomIndex], updatedProduct, "Updated some different product");
        }

        [Test]
        public void UpdateProductFail()
        {
            // Find some id that does not exist
            int id;
            do
            {
                id = new Random().Next();
            } while (mockManager.Products.Any(prod => prod.ID == id));
            Product updatedProduct = new Product() { ID = 9000, ProductName = "Updated product", ImgUri = "NewUri", Price = 999.99f, Description = "NewDesc" };

            var result = api.Put(id, updatedProduct) as NotFoundObjectResult;

            Assert.IsNotNull(result, "Operation doesn't have NotFound Result");
            Assert.AreEqual(404, result.StatusCode, "Retrieve operation has wrong result code");
        }
    }
}