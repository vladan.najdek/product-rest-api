﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonData;

namespace EshopAPIUnitTest
{
    class MockProductManager: IProductManagementService
    {
        public List<Product> Products { get; set; } = new List<Product>()
        {
            new Product() {ID = 1, ProductName = "Product1", ImgUri = "ImgUri1", Price = 9.99f, Description = "Blabla1"},
            new Product() {ID = 2, ProductName = "Product2", ImgUri = "ImgUri2", Price = 10.99f, Description = "Blabla2"},
            new Product() {ID = 3, ProductName = "Product3", ImgUri = "ImgUri3", Price = 11.99f, Description = "Blabla3"},
            new Product() {ID = 4, ProductName = "Product4", ImgUri = "ImgUri4", Price = 8.99f, Description = "Blabla4"},
            new Product() {ID = 5, ProductName = "Product5", ImgUri = "ImgUri5", Price = 6.99f, Description = "Blabla5"}
        };
        public List<Product> GetAllProducts()
        {
            return Products;
        }

        public List<Product> GetPageOfProducts(int page, int pagination)
        {
            List<Product> returnList = new List<Product>();
            for (int ii = (page-1) * pagination; ii <= page*pagination - 1; ii++)
            {
                if (Products.ElementAtOrDefault(ii) != null) returnList.Add(Products[ii]);
            }
            return returnList;
        }

        public Product GetProduct(int id)
        {
            return Products.Where(p => p.ID == id).FirstOrDefault();
        }

        public bool UpdateProduct(int id, Product updatedProduct)
        {
            var product = Products.Where(p => p.ID == id).FirstOrDefault();
            if (product == null) return false;
            Products[Products.FindIndex(ind => ind.Equals(product))] = updatedProduct;
            return true;
        }
    }
}
